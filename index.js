/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './src';
import {name as appName} from './app.json';
import { configApp as appSettings } from './src/config/Config';

AppRegistry.registerComponent(appName, () => App);
AppRegistry.registerComponent(appSettings, () => configApp);
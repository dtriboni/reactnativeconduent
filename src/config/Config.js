import { StatusBar } from 'react-native';

/**
* Custom styles and text for App
* set colors, client ID, OneSignal ID and urlBase
*/ 
export const configApp = {

    "barStyle": "light-content", //color for statusBar (IOS and Android)

    "mainTitle": "GEBSAPrev", //header Title for App

    "backgroundColor": "#2980b9", //color for background header and Login Activity

    "headerTintColor": "#FFF", //color for text in header

    "foreColor": "#3498db", //color for button LOGIN and border fields

    "fontColor": "#FFF", //color fonts

    "loginTitle": "Acompanhe a evolução do seu plano", //title below image in login Activity

    "urlBase": "https://www.portal-hro.com.br/ws/", //System URL base

    "idClient": 62, //Client ID 

    "appIDPush": "0e98359d-3601-407a-9d8b-c2847331e5f3"  //OneSignal app ID
}

StatusBar.setBackgroundColor(configApp.backgroundColor);
StatusBar.setBarStyle(configApp.barStyle);
import React, { Component } from 'react';
import { View, WebView, ActivityIndicator, StyleSheet } from 'react-native';
import { configApp } from '../config/Config';

/**
* Class for Forgot Password Activity
* Extends components
*/
export default class Forgot extends Component {
    

    /**
    * Class Forgot Constructor
    * Prepare state values
    */
    constructor () { 
        super();
        this.state = { visible: true, animating: true }; 
    } 


    /**
    * Static method for Forgot header with title 
    * and navigation buttons
    */
    static navigationOptions = {
        title: "Lembrar Senha",
        headerTintColor: configApp.headerTintColor,
        headerStyle: {backgroundColor: configApp.backgroundColor},
    }
     

    /**
    * Hide loading spinner
    * 
    */
    hideSpinner = () => {
        this.setState({ visible: false });
        this.setState({ animating: false });        
    };


    /**
    * Render Forgot Activity
    * 
    */
    render() {
        const animating = this.state.animating;
        return (
            <View style={styles.container}>
                <WebView
                onLoad={() => this.hideSpinner()} 
                source={{ uri: configApp.urlBase + "Guest.aspx?t=altsenha&e=" + configApp.idClient }}
                />
                {this.state.visible && (
                    <ActivityIndicator
                        animating = {animating}
                        color = '#FFF'
                        size = "large"
                        style={{position:'absolute', backgroundColor:'rgba(52, 52, 52, 0.8)', left:0, right:0, bottom:0, top:0 }}
                    />
                )}
            </View>
        )
    }
}


/**
* Custom styles for Forgot Activity
* 
*/
const styles = StyleSheet.create ({
    container: {
       flex: 1
    }
})
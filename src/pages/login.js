import React, { Component } from 'react';
import { View, Alert, Text, TextInput, TouchableOpacity, KeyboardAvoidingView, ActivityIndicator, Image, StyleSheet, AsyncStorage } from 'react-native';
import api from '../services/api';
import { CheckBox } from 'react-native-elements'
import OneSignal from 'react-native-onesignal';
import { configApp } from '../config/Config';

/**
* Class for Login Activity
* Extends components and OneSignal for PushNotification
*/
export default class Login extends Component {


    /**
    * Class Login Constructor
    * Prepare state values and OneSignal initialization
    */
    constructor()
    {
        super();
        this.state={
            email: '',
            password: '',
            date: '',
            switchValue: false,
            visible: false, 
            animating: false
        }
        OneSignal.setLogLevel(6, 0);
        OneSignal.init(configApp.appIDPush, {kOSSettingsKeyAutoPrompt : true});
    }


    /**
    * Static method for Login header without title 
    * and navigation buttons
    */
    static navigationOptions = {
        header: null
    }


    /**
    * Prepare OneSignal listeners after Activity
    * is loaded
    */
    async componentDidMount() 
    {    
        //await AsyncStorage.setItem('_appNotifications', undefined); 
        OneSignal.inFocusDisplaying(2);

        this.onReceived = this.onReceived.bind(this);
        this.onIds = this.onIds.bind(this);
  
        OneSignal.addEventListener('received', this.onReceived);
        OneSignal.addEventListener('ids', this.onIds);
    }


    /**
    * Read AsyncStorage auto login before Login Activity
    * is loaded
    */
    async componentWillMount() 
    {
        try {
            const value = await AsyncStorage.getItem('_autoLogin');
            if (value == "1")
            {
                const { navigate } = this.props.navigation;
                navigate("Main");
            }
        } catch (error) {
            console.warn("Error retrieving data" + error);
        }
    }


    /**
    * OneSignal received notifications
    * Save AsyncStorage pushes on device
    */
    async onReceived(notification) 
    {
        var that = this;
        var date = new Date().getDate()+''; //Current Date
        var month = (new Date().getMonth() + 1) + ''; //Current Month
        var year = new Date().getFullYear(); //Current Year
        var hours = new Date().getHours()+''; //Current Hours
        var min = new Date().getMinutes()+''; //Current Minutes
        var sec = new Date().getSeconds()+''; //Current Seconds
        
        that.setState({
            date:
              date.padStart(2, '0') + '/' + 
              month.padStart(2, '0') + '/' + 
              year + ' ' + 
              hours.padStart(2, '0') + ':' + 
              min.padStart(2, '0') + ':' + 
              sec.padStart(2, '0'),
        });

        try {

            var data = [];
            var json = {
                    "_id": "",
                    "title": "",
                    "description": "",
                    "date": ""
                };
            const response = await AsyncStorage.getItem('_appNotifications');
            var notifications = JSON.parse(response);
            if (notifications != null)
            {
                notifications.forEach(element => {
                    json._id = element._id;
                    json.title = element.title;
                    json.description = element.description;
                    json.date = element.date;
                    data.push(JSON.parse(JSON.stringify(json)));
                });
            }
            if (notification.payload !== undefined)
            {
                json._id = notification.payload.notificationID;
                json.title = notification.payload.title;
                json.description = notification.payload.body;
                json.date =  this.state.date;
                data.push(JSON.parse(JSON.stringify(json)));
            }

            data.sort(function(a,b){
                // Turn your strings into dates, and then subtract them
                // to get a value that is either negative, positive, or zero.
                return new Date(b.date) - new Date(a.date);
            });

            await AsyncStorage.setItem('_appNotifications', JSON.stringify(data)); 

        } catch (error) {
            console.warn("Error retrieving data" + error);
        }        
    }
  

    /**
    * OneSignal received Device ID
    * TODO
    */
    onIds(device) 
    {
      console.log('Device info: ', device);
    }


    /**
    * Get checkbox value to auto login state
    * save value to AsyncStorage device
    */
    toggleSwitch = async (value) => {
        if (value == false)
        {
            this.setState({switchValue: true})
            await AsyncStorage.setItem('_autoLogin', "1");
        }else{
            this.setState({switchValue: false})
            await AsyncStorage.setItem('_autoLogin', "0");
        }
    }


    /**
    * Update form values 
    * populate distinct states (email and password)
    */
    updateValue(text, field)
    {
        if (field == 'email')
        {
            this.setState({
                email: text
            })
        } else if (field == 'password')
        {
            this.setState({
                password: text 
            })
        }
    }


    /**
    * Submit the form to HRO API
    * Save user ID and Situation
    */
    async submit()
    {
        let collection = {
            UserName: this.state.email,
            Password: this.state.password
        }

        if (collection.UserName == "" || 
            collection.Password == "")
        {
            Alert.alert(
                'Atenção',
                'Informe seus dados de acesso para continuar!',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                {cancelable: false},
              );
        
        }else{

            this.showSpinner();
            let body = "UserName=00"+configApp.idClient+collection.UserName+"&Password="+collection.Password; 
            const response = await api.post(configApp.urlBase + "Users.asmx/LoginJSON", body);

            if (response.data.status == "false")
            {
                this.hideSpinner();
                Alert.alert(
                    'Atenção',
                    response.data.mensagem,
                    [
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    {cancelable: false},
                );
            
            }else{
                var _id = response.data.id;
                this.hideSpinner();
                const { navigate } = this.props.navigation;
                await AsyncStorage.setItem('_idClient', _id);

                this.setState({ email: '' });
                this.setState({ password: '' });

                OneSignal.sendTags({"cpf" : response.data.cpf, "situacao" : response.data.situacao, "motivo" : response.data.motivo});
                
                navigate("Main");
            }
        }
    }


    /**
    * Hide loading spinner
    * 
    */
    hideSpinner = () => {
        this.setState({ visible: false });
        this.setState({ animating: false });        
    };


    /**
    * Show loading spinner
    * 
    */
    showSpinner = () => {
        this.setState({ visible: true });
        this.setState({ animating: true });        
    };

  
    /**
    * Render Login Activity
    * 
    */
    render() 
    {
        var img;
        switch(configApp.mainTitle)
        {
            case "AVONPREV": img = require('../images/logos/logo_AVONPREV.png'); break;
            case "CarrefourPrev": img = require('../images/logos/logo_CarrefourPrev.png'); break;
            case "EMBRAERPREV": img = require('../images/logos/logo_EMBRAERPREV.png'); break;
            case "FIPECq": img = require('../images/logos/logo_FIPECq.png'); break;
            case "GEBSAPrev": img = require('../images/logos/logo_GEBSAPrev.png'); break;
            case "Prevmon": img = require('../images/logos/logo_Prevmon.png'); break;
            case "PrevPepsico": img = require('../images/logos/logo_PrevPepsico.png'); break;
            case "RBSPrev": img = require('../images/logos/logo_RBSPrev.png'); break;
        }
        const animating = this.state.animating;
        return (
            <KeyboardAvoidingView 
                behavior="padding" 
                style={styles.container}>
               <View style={styles.formContainer}> 
                <View  
                    style={styles.logoContainer}> 
                    <Image 
                    style={styles.logo} 
                    source={img} />
                    <Text 
                        style={styles.title}>{ configApp.loginTitle }</Text>
                </View>
                <TextInput  
                    style={styles.input} 
                    placeholder="CPF"
                    placeholderTextColor="#999"
                    returnKeyType="next"
                    value={this.state.email}
                    onChangeText={(text) => this.updateValue(text, 'email')}
                    autoCapitalize="none"
                    autoCorrect={false}
                    onSubmitEditing={() => this.passwordInput.focus()}>

                </TextInput>
                <TextInput 
                    style={styles.input}
                    secureTextEntry
                    returnKeyType="go"
                    value={this.state.password}
                    onChangeText={(text) => this.updateValue(text, 'password')}
                    ref={(input) => this.passwordInput = input}
                    placeholder="Senha"
                    placeholderTextColor="#999">

                </TextInput>
                <CheckBox
                    containerStyle={styles.switchContainer}
                    checkedIcon="dot-circle-o"
                    uncheckedIcon="circle-o" 
                    checked={this.state.switchValue}
                    onPress={() => this.toggleSwitch(this.state.switchValue)} />
                    <Text style={styles.buttonSwitch}>Mantenha-me logado</Text>
                <TouchableOpacity 
                    onPress={() => this.submit()}
                    style={styles.buttonContainer}>
                    <Text style={styles.buttonText}>LOGIN</Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    onPress={() => this.props.navigation.navigate("forgotScreen")}
                    style={styles.buttonForgot}>
                    <Text style={styles.buttonTextForgot}>Esqueceu sua senha?</Text>
                </TouchableOpacity>
               </View>
               {this.state.visible && (
                    <ActivityIndicator
                        animating = {animating}
                        color = '#FFF'
                        size = "large"
                        style={{position:'absolute', backgroundColor:'rgba(52, 52, 52, 0.8)', left:0, right:0, bottom:0, top:0 }}
                    />
                )}
            </KeyboardAvoidingView>
        )
    }
}


/**
* Custom styles for Login Activity
* 
*/
const styles = StyleSheet.create({
    container: {
        padding: 20,
        flex: 1,
        backgroundColor: configApp.backgroundColor,
    },
    logoContainer: {
        //marginTop: 60,
        alignItems: 'center',
        flexGrow: 3,
        justifyContent: 'center'
    },
    logo: {
       marginTop: 10, 
       width: 242, 
       height: 89
    },
    formContainer: {
        marginBottom: 50,
        flexGrow: 4,
        justifyContent: 'center'
    },
    title: {
        color: '#FFF',
        marginTop: 1,
        marginBottom: 15,
        width: '100%',
        textAlign: 'center',
        fontSize: 16,
        fontWeight: 'bold',
        opacity: 0.9
    },
    input: {
        height: 40,
        fontWeight: "bold",
        fontSize: 16,
        backgroundColor: "#FFF",
        borderColor: configApp.foreColor,
        borderWidth: 2,
        borderRadius: 5,
        marginBottom: 15,
        paddingHorizontal: 10
    },
    buttonContainer: {
        height: 40,
        borderRadius: 5,
        marginTop: 20,
        marginBottom: 10,
        borderWidth: 2,
        borderColor: configApp.foreColor,
        backgroundColor: configApp.foreColor,
        justifyContent: "center",
        alignItems: "center",
    },
    buttonText: { 
        fontSize: 18,
        color: "#FFF",
        fontWeight: "bold",
        textAlign: 'center' 
    },
    switchContainer: {
        marginTop:-10,
        height: 60,
        left: -15,
        zIndex: 100 
    },
    buttonSwitch: { 
        fontSize: 18,
        color: "#FFF",
        fontWeight: "bold",
        textAlign: 'left',
        paddingLeft: 35,
        marginTop: -53
    },
    buttonForgot: {
        height: 25,
        marginBottom: 5,
        alignItems: "center",
    },
    buttonTextForgot: {
        fontSize: 18,
        color: "#FFF",
        fontWeight: "bold",
        textAlign: 'center' 
    }
})
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, FlatList, AsyncStorage } from 'react-native';
import { Icon } from 'react-native-elements';
import { configApp } from '../config/Config';
 
/**
* Class for Notifications Activity
* Extends components
*/
export default class Notifications extends Component {
    
    /**
    * Static method for Notifications header with title 
    * and navigation buttons
    */
    static navigationOptions = {
        title: "Notificações",
        headerTintColor: configApp.headerTintColor,
        headerStyle: {backgroundColor: configApp.backgroundColor}
    }


    /**
    * Set notifications JSON Object
    */
    state = {
        notifs: [],
        notif: false
    }


    /**
    * Load notifications in AsyncStorage 
    * set JSON Object
    */ 
    loadNotifications = async () => {
        const response = await AsyncStorage.getItem("_appNotifications");
        const notifications = JSON.parse(response);
        if (notifications != null && Object.keys(notifications).length > 0)
        {
            this.setState({notif: true}); 
        }
        this.setState({notifs: notifications}); 
    };


    /**
    * Prepare to Load Notifications after Activity
    * is loaded
    */
    componentDidMount() {
        this.loadNotifications();
    }


    /**
    * Remove notification by clicking
    * 
    */
    async removeNotification(id) 
    {
        var data = [];
        var json = {
                "_id": "",
                "title": "",
                "description": "",
                "date": ""
            };
        const response = await AsyncStorage.getItem('_appNotifications');
        var notifications = JSON.parse(response);
        if (notifications != null)
        {
            notifications.forEach(element => {
                if (element._id != id)
                {
                    json._id = element._id;
                    json.title = element.title;
                    json.description = element.description;
                    json.date = element.date;
                    data.push(JSON.parse(JSON.stringify(json)));
                }
            });
            await AsyncStorage.setItem('_appNotifications', JSON.stringify(data)); 
            this.loadNotifications();
        }
    }


    /**
    * Render List Notification
    * 
    */
    renderItem = ({item}) => (
        <View style={styles.notifContainer}>
            <Text style={styles.notifTitle}>{item.title}</Text>
            <Text style={styles.notifDate}>{item.date}</Text>
            <Text style={styles.notifDescription}>"{item.description}"</Text>
            <TouchableOpacity 
                onPress={() => this.removeNotification(item._id)}
                style={{position:'absolute', right:10, top:10 }}>
                <Icon name='delete' color={configApp.backgroundColor} /> 
            </TouchableOpacity>
        </View>
    );


    /**
    * Render Notification Activity
    * 
    */
    render() {
        return (
            <View style={styles.container}>
                {!this.state.notif && (
                    <View style={styles.noNotifContainer}>
                        <Icon color={configApp.backgroundColor} name='notifications' style={styles.noNotifIcon} />
                        <Text style={styles.noNotifDescription}>Nenhuma notificação recebida!</Text>
                    </View>
                )}
                <FlatList
                    contentContainerStyle={styles.list}
                    data={this.state.notifs}
                    keyExtractor={item => item._id}
                    renderItem={this.renderItem} />
            </View>
        )
    }
} 


/**
* Custom styles for Notification Activity
* 
*/
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#EEE",
    },
    list: {
        padding: 10
    },
    buttonContainer: {
        height: 40,
        marginTop: 20,
        marginBottom: 10,
        borderWidth: 2,
        justifyContent: "center",
        alignItems: "center",
    },
    notifContainer: {
        backgroundColor: "#FFF",
        borderWidth: 1, 
        minHeight: 100,
        borderColor: configApp.foreColor,
        borderRadius: 5,
        padding: 10,
        marginBottom: 10
    },
    noNotifContainer: { 
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
        flex: 4
    },
    notifTitle: {
        marginTop: -3,
        fontSize: 18,
        fontWeight: "bold",
        color: "#333"
    },
    notifDate: {
        fontSize: 12,
        color: "#666",
        marginTop: -3,
        lineHeight: 24
    },
    notifDescription: {
        fontSize: 14,
        fontStyle: 'italic',
        color: "#666",
        marginTop: -3,
        lineHeight: 24
    },
    noNotifDescription: {
        fontSize: 14,
        color: configApp.backgroundColor
    }
})
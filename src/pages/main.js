import React, { Component } from 'react';
import { View, WebView, AsyncStorage, StyleSheet, ActivityIndicator } from 'react-native';
import { configApp } from '../config/Config';

/**
* Class for Main Activity
* Extends components
*/
export default class Main extends Component { 


    /**
    * Class Main Constructor
    * Prepare state values
    */
    constructor(props) {
        super(props);
        this.state = {
            myKey: null,
            visible: true, 
            animating: true
        }
    }


    /**
    * Static method for Main header with title 
    * and navigation buttons
    */
    static navigationOptions = {
        title: configApp.mainTitle,
        headerTintColor: configApp.headerTintColor,
        headerStyle: {backgroundColor: configApp.backgroundColor}
    }


    /**
    * Read AsyncStorage User ID before Login Activity
    * is loaded 
    */
    async componentWillMount() 
    {
        try {
            const value = await AsyncStorage.getItem('_idClient');
            if (value == null){
                let keys = ['_idClient', '_autoLogin'];
                await AsyncStorage.multiRemove(keys, (err) => { 
                  // keys _idClient and _autoLogin removed, if they existed
                  // key _appNotifications will not removed
                });
                this.props.navigation.navigate('loginScreen');
            }else{
                this.setState({myKey: value});
            }
        } catch (error) {
            console.warn("Error retrieving data" + error);
        }
    }

    componentDidMount() 
    {
        this.setState({ visible: true });
        this.setState({ animating: true }); 
    }
 
    
    /**
    * Hide loading spinner
    * 
    */
    hideSpinner = () => {
        this.setState({ visible: false });
        this.setState({ animating: false });        
    };


    /**
    * Render Main Activity
    * 
    */
    render() 
    {    
        const animating = this.state.animating;
            return (
                <View style={styles.container}>
                    <WebView
                    onLoad={() => this.hideSpinner()} 
                    source={{ uri: configApp.urlBase + "ProfileWeb.aspx?id=" + this.state.myKey }}
                    />
                    {this.state.visible && (
                        <ActivityIndicator
                            animating = {animating}
                            color = '#FFF'
                            size = "large"
                            style={{position:'absolute', backgroundColor:'rgba(52, 52, 52, 0.8)', left:0, right:0, bottom:0, top:0 }}
                        />
                    )}
                </View>
            )
    }
}


/**
* Custom styles for Main Activity
* 
*/
const styles = StyleSheet.create ({
    container: {
      flex: 1
    }
})
import React from 'react';
import Routes from './routes';
import './config/Config';

const App = () => <Routes />;

export default App;
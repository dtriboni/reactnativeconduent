import React, { Component } from 'react'
import { TouchableOpacity, AsyncStorage, StyleSheet, Alert, Animated, Easing, View, Text, Image, Dimensions } from 'react-native'
import { StackNavigator, DrawerNavigator } from 'react-navigation';
import { Icon } from 'react-native-elements';
import Login from './pages/login';
import Main from './pages/main';
import Forgot from './pages/forgot';
import { configApp } from './config/Config';
import Notifications from './pages/notifications';

/**
* Animation for Drawer menu
* 
*/
const noTransitionConfig = () => ({
    transitionSpec: {
      duration: 0,
      timing: Animated.timing,
      easing: Easing.step0
    }
})


/**
* Class for Custom Side Menu (Drawer)
* Extends components
*/
class Custom_Side_Menu extends Component {


    /**
    * Exit App
    * Clear all AsyncStorage (except for Notifications)
    */
    exitApp() 
    {
      Alert.alert(
        'Atenção',
        'Deseja sair do App?',
        [
          { text: 'Não', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          { text: 'Sim', onPress: async () => {

            try {
                let keys = ['_idClient', '_autoLogin'];
                await AsyncStorage.multiRemove(keys, (err) => { 
                  // keys _idClient and _autoLogin removed, if they existed
                  // key _appNotifications will not removed
                });
                this.props.navigation.navigate('loginScreen')
            } catch (error) {
                console.warn("Error retrieving data" + error);
            }

          }},
        ],
        {cancelable: false},
      );
    }


    /**
    * Render Main Menu
    * 
    */
    render()  
    {
       
        return (
            <View style={styles.sideMenuContainer}> 
              <View style={{width: '100%'}}>
      
                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 30, marginLeft: 20}}>
                <Icon name='person' color={configApp.foreColor} style={styles.sideMenuIcon} />
                <Text style={styles.menuText} onPress={() => { this.props.navigation.navigate('Main') }} > Minha Conta </Text>
                </View>
      
                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 30, marginLeft: 20}}>
                <Icon name='notifications' color={configApp.foreColor} style={styles.sideMenuIcon} />
                <Text style={styles.menuText} onPress={() => { this.props.navigation.navigate('Notification') }} > Notificações </Text>
                </View>
      
                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 30, marginLeft: 20}}>
                <Icon name='exit-to-app' color={configApp.foreColor} style={styles.sideMenuIcon} />
                <Text style={styles.menuText} onPress={() => { this.exitApp() }} > Sair </Text>
                </View>
      
              </View>
              <View style={{ width: '100%', height: 1, backgroundColor: '#e2e2e2', marginTop: 15}} />
      
            </View>
        );
    }
  }

/**
* Load Drawer Menu (for Main Activity)
* 
*/
const DrawerStack = DrawerNavigator({
    Main: { screen: Main },
    Notification: {screen: Notifications,  
      navigationOptions: {
        title: "Notificações",
        headerTintColor: configApp.headerTintColor,
        headerStyle: {backgroundColor: configApp.backgroundColor}
      }
    }
},{
    contentComponent: Custom_Side_Menu,
    drawerWidth: Dimensions.get('window').width - 90,
})


/**
* Load Stack Navigation for DrawerMenu
* 
*/  

var img;
switch(configApp.mainTitle)
{
    case "AVONPREV": img = require('../src/images/logos/logo_AVONPREV.png'); break;
    case "CarrefourPrev": img = require('../src/images/logos/logo_CarrefourPrev.png'); break;
    case "EMBRAERPREV": img = require('../src/images/logos/logo_EMBRAERPREV.png'); break;
    case "FIPECq": img = require('../src/images/logos/logo_FIPECq.png'); break;
    case "GEBSAPrev": img = require('../src/images/logos/logo_GEBSAPrev.png'); break;
    case "Prevmon": img = require('../src/images/logos/logo_Prevmon.png'); break;
    case "PrevPepsico": img = require('../src/images/logos/logo_PrevPepsico.png'); break;
    case "RBSPrev": img = require('../src/images/logos/logo_RBSPrev.png'); break;
}

const DrawerNavigation = StackNavigator({
    DrawerStack: { screen: DrawerStack } 
},{
  
    navigationOptions: ({navigation}) => ({
        //title: configApp.mainTitle,
        
        headerTitle: (
          <View style={{flex: 1, alignItems: 'center'}}>
          <Image resizeMode="cover" 
                 source={img} 
                 style={{width: 109, height: 40, top:0 }}/>
          </View>
         
        ),
       
        headerTintColor: configApp.headerTintColor, 
        headerStyle: {padding: 8, backgroundColor: configApp.backgroundColor},
        gesturesEnabled: true,
        headerLeft: <TouchableOpacity style={styles.menu} onPress={() => {
            navigation.openDrawer();
        }}><Icon name='menu' color="#FFF" /></TouchableOpacity>,
        headerRight: <TouchableOpacity style={styles.notif} onPress={() => {
            navigation.navigate("Notification");
        }}><Icon name='notifications' color="#FFF" /></TouchableOpacity> 
    })
})


/**
* Load Login and Forgot Stack Navigation
* 
*/ 
const LoginStack = StackNavigator({
    loginScreen: { screen: Login },
    forgotScreen: {screen: Forgot } 
})


/**
* Load Stack Navigation for All Activities
* set custom headers for any Activity
*/ 
const PrimaryNav = StackNavigator({
    loginStack: { screen: LoginStack },
    drawerStack: { screen: DrawerNavigation }
}, {
    headerMode: 'none',
    title: 'Main',
    initialRouteName: 'loginStack',
    transitionConfig: noTransitionConfig
})


/**
* Export Main Navigation 
* based on Routes
*/ 
export default PrimaryNav;


/**
* Custom styles for Menus
* 
*/
const styles = StyleSheet.create({
  menu: {
      marginLeft: 10,
      color: "#FFF"
  },
  notif: {
      marginRight: 10,
      color: "#FFF"
  },
  sideMenuContainer: {
      width: '100%',
      height: '100%',
      backgroundColor: '#fff', 
      
  },
  sideMenuIcon:
  {
      resizeMode: 'contain',
      alignSelf: 'center',
      width: 32, 
      height: 32,  
      marginRight: 10,
      marginLeft: 20
    
  },
  headerImg: {
    resizeMode: 'contain',
    alignItems: 'center',
    alignSelf:'center',
    width: 109,   
    height: 40,  
  },
  menuText:{
      fontSize: 17,
      color: configApp.backgroundColor,
  }
});  